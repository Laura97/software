import sys

#import things from matplotlib and setup for use with PyQt5
import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

#import things from PyQt5
from PyQt5.QtWidgets import (QMainWindow, QApplication, QAction, QHBoxLayout,
                             QWidget, qApp, QFrame, QStyleFactory, QSplitter,
                             QTableWidget, QTableWidgetItem, QLineEdit, QLabel,
                             QSizePolicy)
from PyQt5.QtCore import (Qt, QTimer)

matrix = [['Time', 'Temperature', 'Wind Speed'], ['11:00AM', 20, 9],
          ['12:00PM', 20, 11], ['1:00PM', 19, 11], ['2:00PM', 17, 12],
          ['3:00PM', 17, 12], ['4:00PM', 17, 11], ['5:00PM', 17, 9],
          ['6:00PM', 16, 6], ['7:00PM', 16, 4], ['8:00PM', 16, 2],
          ['9:00PM', 15, 5], ['10:00PM', 15, 8], ['11:00PM', 15, 10],
          ['12:00AM', 15, 15], ['1:00AM', 15, 17], ['2:00AM', 14, 18],
          ['3:00AM', 14, 19], ['4:00AM', 13, 20], ['5:00AM', 12, 20],
          ['6:00AM', 12, 20], ['7:00AM', 12, 20], ['8:00AM', 13, 23],
          ['9:00AM', 15, 27], ['10:00AM', 16, 29]]

#make class for all graphs
class Plot(FigureCanvas):

    def __init__(self, parent = None):
        fig = Figure()
        self.axes = fig.add_subplot(111)
        self.axes.hold(False)
        self.compute_initial_figure()

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

    def computer_initial_figure(self):
        pass

#a static sin graph for display
class Temperature(Plot):

    def compute_initial_figure(self):
        #save x and y values
        plot = []
        for i in range(len(matrix)):
            if i == 0:
                continue
            else:
                value = ''
                for char in matrix[i][0]:
                    if char is not ':':
                        value = value + char
                    else:
                        value = float(value)
                        if matrix[i][0][-2:] == 'PM' and value != 12:
                            value += 12
                        if matrix[i][0][-2:] == 'AM' and value == 12:
                            value = value - 12
                        break
                plot.append((value, matrix[i][1]))
        
        self.axes.plot(sorted(plot))
        print(plot)
                        

#make widget
class App(QWidget):
    
    def __init__(self):
        super().__init__()
        self.initUI()
        
    def initUI(self):
        action = QAction('&Exit', self)
        action.setStatusTip('Exit Window')
        action.triggered.connect(qApp.quit)

        hbox = QHBoxLayout(self)

        #the table
        topleft = QTableWidget()
        topleft.setRowCount(len(matrix))
        topleft.setColumnCount(len(matrix[0]))
        #pulling data from the matrix
        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                line = matrix[i][j]
                if type(line) is int:
                    line = str(line)
                topleft.setItem(i, j, QTableWidgetItem(QLineEdit
                                                       (line).text()))
 
        topright = QFrame(self)
        plot = Temperature(topright)
        topright.setFrameShape(QFrame.StyledPanel)

        bottom = QLabel("Here be Dragons")
        bottom.setFrameShape(QFrame.StyledPanel)
        

        splitter1 = QSplitter(Qt.Horizontal)
        splitter1.addWidget(topleft)
        splitter1.addWidget(topright)

        splitter2 = QSplitter(Qt.Vertical)
        splitter2.addWidget(splitter1)
        splitter2.addWidget(bottom)

        hbox.addWidget(splitter2)
        self.setLayout(hbox)

        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('Weather Station GUI')     
        self.show()
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = App()
    sys.exit(app.exec_())
